
public class Blender
{
	private boolean isPluggedIn;
	private double heightInHumanLegs;
	private double radiusInHumanHands;
	private double limbsUsed;
	
	public Blender(boolean isPluggedIn, double heightInHumanLegs, double radiusInHumanHands)
	{
		this.isPluggedIn = isPluggedIn;
		this.heightInHumanLegs = heightInHumanLegs;
		this.radiusInHumanHands = radiusInHumanHands;
	}
	
	public double howManyLimbsCanItBlend()
	{
		double h = this.heightInHumanLegs;
		double r = convertHHtoHL(this.radiusInHumanHands);
		
		double volumeInLimbs = 3.14 * r * r * h;
		
		return volumeInLimbs;
	}
	
	public void plugInBlender()
	{
		if (this.isPluggedIn != true)
		{
			this.isPluggedIn = true;
			System.out.println("Congrats! You've plugged in a blender!");
		}
		else
		{
			System.out.println("What are you kidding or something?");
		}
	}
	
	public static double convertHHtoHL(double radiusInHumanHands) //convert human hands (hh) to human legs (hl)
	{
		return (radiusInHumanHands/5);
	}
	
	public void startBlending(double limbsAmount) //commences production of forbidden soup if prerequisites are met.
	{
		if (limbsAmount > this.howManyLimbsCanItBlend())
		{
			System.out.println("That's too many limbs! Don't be greedy!");
		}
		else
		{
			this.limbsUsed = limbsAmount;
			if (this.isPluggedIn == true)
			{
				System.out.println("(Insert grinding noise here)! Bzt! There you go! Your [FORBIDDEN SOUP] has been completed!");
			}
			else
			{
				System.out.println("Plug in your blender first!");
			}
		}
	}
	
	public void warmForbiddenSoup()  //heats of forbidden soup as needed
	{
		if (this.limbsUsed > 0) 
		{
			System.out.println("Your forbidden soup contains " + this.limbsUsed + " limbs. Allow me to warm it up it...");
			System.out.println("Your forbidden soup is now warm! Enjoy!");
		}
		else
		{
			System.out.println("Uh... You need to make your soup before you heat it...");
		}
	}
	
	public boolean getIsPluggedIn(){
		return this.isPluggedIn;
	}
	
	public double getHeightInHumanLegs(){
		return this.heightInHumanLegs;
	}
	
	public double getRadiusInHumanHands(){
		return this.radiusInHumanHands;
	}
	
	public double getLimbsUsed(){
		return this.limbsUsed;
	}
	
	
	public void setIsPluggedIn(boolean newIsPluggedIn){
		this.isPluggedIn = newIsPluggedIn;
	}
	
	public void setRadiusInHumanHands(double newRadiusInHumanHands){
		this.radiusInHumanHands = newRadiusInHumanHands;
	}
	
	public void setLimbsUsed(double newLimbsUsed){
		this.limbsUsed = newLimbsUsed;
	}
	
}