import java.util.Scanner;

public class ApplianceStore
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Blender[] blenders = new Blender[2];
		for (int i = 0; i < blenders.length; i++)
		{
			System.out.println("Blender number " + i);
			System.out.println("How many human legs long is it?");
			System.out.println("What is the radius of this blender in human hands?");
			blenders[i] = new Blender(false, scan.nextDouble(), scan.nextDouble());
			System.out.println();
		}
		System.out.println("Blender " + (1) + " is it plugged in? " + blenders[1].getIsPluggedIn() + "\nhow long is it? " + blenders[1].getHeightInHumanLegs() + "\nWhat is it's radius? " + blenders[1].getRadiusInHumanHands() + "\nHow many limbs are inside? " + blenders[1].getLimbsUsed());
		System.out.println();
		if (!blenders[1].getIsPluggedIn())
		{
			System.out.println("Do you want to plug in and use your blender? 1 for yes or 0 for no");
			int input = scan.nextInt();
			if (input == 1)
			{
				blenders[1].plugInBlender();
				
				System.out.println("How many limbs do you want to blend?");
				double limbs = scan.nextDouble();
				blenders[1].startBlending(limbs);
			}
			else
			{
				System.out.println("Damn alright then. Bye.");
			}
		}
		else
		{
			System.out.println("How many limbs do you want to blend?");
			double limbs = scan.nextDouble();
			blenders[1].startBlending(limbs);
		}
		
		System.out.println();
		System.out.println("Blender " + (1) + " is it plugged in? " + blenders[1].getIsPluggedIn() + "\nhow long is it? " + blenders[1].getHeightInHumanLegs() + "\nWhat is it's radius? " + blenders[1].getRadiusInHumanHands() + "\nHow many limbs are inside? " + blenders[1].getLimbsUsed());
	}
}
